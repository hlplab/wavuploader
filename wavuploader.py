#!/usr/bin/env python

#Author: Andrew Watts
#
#    Copyright 2011-2012 Andrew Watts and
#        the University of Rochester BCS Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version 2.1 as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.
#

from __future__ import print_function

from os import makedirs
import os.path
import ConfigParser
#from pprint import pprint
from hashlib import sha224
import logging

from webob import Request, Response
from webob.exc import HTTPException, HTTPBadRequest, HTTPForbidden, HTTPOk

basepath = os.path.dirname(__file__)
cfg = ConfigParser.SafeConfigParser()
cfg.read(os.path.join(basepath, 'uploader.cfg'))
savebase = cfg.get('files', 'path')
if savebase != '':
    basepath = savebase
production = cfg.getboolean('mode', 'production')


class WAVUploader(object):
    """
    WSGI compatible class to respond to WAV upload from WAMI
    """

    def __init__(self, app=None):
        # this way if running standalone, gets app, else doesn't need it
        self.app = app

        # logger needs to be set up here, or when handler is added in __call__
        # it will print each message once for each time __call__ has been called
        self.logger = logging.getLogger()
        if production:
            self.logger.setLevel(logging.ERROR)
        else:
            self.logger.setLevel(logging.INFO)

        ch = logging.StreamHandler()  # default stream is stderr
        formatter = logging.Formatter('%(asctime)s %(module)s [[%(levelname)s]] %(message)s', '%a %d %b %y %T')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def __call__(self, environ, start_response):

        errors = environ['wsgi.errors']
        # there should only be one handler and we're changing its output stream
        # from stderr to wsgi.errors
        self.logger.handlers[0].stream = errors

        req = Request(environ)

        try:
            for k in ('workerId', 'assignmentId', 'hitId', 'hash'):
                if not k in req.params:
                    raise HTTPBadRequest('Missing key: {}'.format(k))

            # Nothing should get saved in preview mode
            if req.params['assignmentId'] == 'ASSIGNMENT_ID_NOT_AVAILABLE':
                raise HTTPBadRequest('Files cannot be saved in HIT preview')

            # Check that the hash of the amazon params is right
            # This is pretty minimal security, but I'm not sure what else to do
            h = sha224("{}{}{}".format(req.params['workerId'],
                                        req.params['hitId'],
                                        req.params['assignmentId'])).hexdigest()

            # Use this to do a hash parameter instead of a cookie
            if req.params['hash'] != h:
                raise HTTPBadRequest('Bad hash')

            # Use this to do a cookie instead of a hash parameter
            #if req.cookies.get('turkrecord', default=False) != h:
            #    raise HTTPBadRequest('You must have cookies enabled and the cookie value must be valid')

            if 'experiment' in req.params:
                savepath = os.path.join(basepath,
                                        req.params['experiment'],
                                        req.params['workerId'])
            else:
                savepath = os.path.join(basepath,
                                    req.params['workerId'],
                                    req.params['hitId'])

            if req.method == 'POST':
                logging.info("Something was sent to me!")
                logging.info("Content type: {}".format(req.content_type))
                logging.info("Content length: {}".format(req.content_length))
                #for k,v in req.cookies.iteritems():
                #    print("Key: {}, Value: {}".format(k,v))
                #pprint(req.environ)

                if req.content_type != 'audio/x-wav':
                    raise HTTPBadRequest('Only WAV files can be uploaded')

                if 'discard' in req.params:
                    if int(req.params['discard']) == 1:
                        logging.info('File discarded as requested')
                        raise HTTPOk('File discarded as requested')

                if 'filename' in req.params:
                    savefile = req.params['filename'] + ".wav"
                    if not 'experiment' in req.params:
                        savepath = os.path.join(savepath, req.params['assignmentId'])
                else:
                    savefile = req.params['assignmentId'] + ".wav"

                if not os.path.exists(savepath):
                    makedirs(savepath)
                else:
                    # delete old test file first so it isn't hanging around
                    if savefile == 'test.wav' and os.path.isfile(os.path.join(savepath, savefile)):
                        os.unlink(os.path.join(savepath, savefile))

                with open(os.path.join(savepath, savefile), 'wb') as wavfile:
                    wavfile.write(req.body)
                    logging.info("Saved: {}".format(wavfile.name))

                resp = Response()
                return resp(environ, start_response)

            if req.method == 'GET':
                logging.info("Something was requested of me!")
                e = None
                sendfile = ""
                if 'filename' in req.params:
                    if req.params['filename'] == "test":
                        if not 'experiment' in req.params:
                            sendfile = os.path.join(savepath, req.params['assignmentId'], "test.wav")
                        else:
                            sendfile = os.path.join(savepath, "test.wav")
                        logging.info("File to play back: {}".format(sendfile))
                        if os.path.exists(sendfile):
                            with open(sendfile, 'rb') as wavfile:
                                resp = Response()
                                resp.content_type = 'audio/x-wav'
                                resp.body = wavfile.read()
                                return resp(environ, start_response)
                        else:
                            e = HTTPBadRequest('WAV File cannot be found')
                    else:
                        e = HTTPForbidden('Only the level test wav can be played back')
                else:
                    e = HTTPForbidden('WAVs can only be saved, not played back')
                return e(environ, start_response)

        except HTTPException as e:
            logging.error("Encountered exception {} {}".format(e.status, e.detail))
            return e(environ, start_response)

if __name__ == '__main__':
    import os
    from paste import httpserver, fileapp, urlmap

    app = urlmap.URLMap()
    app['/wav_uploader'] = WAVUploader(app)
    app['/crossdomain.xml'] = fileapp.FileApp('crossdomain.xml')
    httpserver.serve(app, host='127.0.0.1', port=8181)
